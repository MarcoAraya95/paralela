#include <iostream>
#include <fstream>
#include <string>
#include <math.h>

#define PI 3.14159265

using namespace std;

/*
TRABAJO AYUDANTIA
REFINAR MALLAS TRIANGULARES

AUTORES:
CAROLINA AVENDA�O B.
MARCO ARAYA C.
IGNACIO GORMAZ T.
*/

void triangulo(int trian[][3], float node[][3], float tr[][10], int i)
{
    float x1(0),x2(0),x3(0),y1(0),y2(0),y3(0),L1(0),L2(0),L3(0),a1(0),a2(0),a3(0);
    int n1(0),n2(0),n3(0),j(0);
    for(j=1;j<i;j++)
    {
        n1 = trian[j][0]; //nodos
        n2 = trian[j][1];
        n3 = trian[j][2];

        x1 = node[n1][1]; // ejes x
        x2 = node[n2][1];
        x3 = node[n3][1];

        y1 = node[n1][2]; // ejes y
        y2 = node[n2][2];
        y3 = node[n3][2];

        L1 = sqrt(pow((x2-x1),2) + pow((y2-y1),2)); // calculo de cada lado
        L2 = sqrt(pow((x3-x1),2) + pow((y3-y1),2));
        L3 = sqrt(pow((x3-x2),2) + pow((y3-y2),2));

        a1 = acos((-pow(L1,2)+ pow(L2,2) + pow(L3,2)) / (2*L2*L3)) * 180.0 / PI; // calculo de los angulos
        a2 = acos((-pow(L2,2)+ pow(L1,2) + pow(L3,2)) / (2*L1*L3)) * 180.0 / PI;
        a3 = acos((-pow(L3,2)+ pow(L1,2) + pow(L2,2)) / (2*L2*L1)) * 180.0 / PI;

        tr[j][0] = j; // numero del triangulo
        tr[j][1] = L1; // lado 1
        tr[j][2] = L2; // lado 2
        tr[j][3] = L3; // lado 3
        tr[j][4] = a1; // angulo 1
        tr[j][5] = a2; // angulo 2
        tr[j][6] = a3; // angulo 3
    }
}

void se_refina(float tr[][10], float angulo, int i) //define los triangulos a refinar
{
    int j(0);
    for (j=1;j<i;j++)
    {
        if (abs(tr[j][4]) <= abs(angulo))
            tr[j][7] = 1; // guarda 1 si el triangulo se refina
        if (abs(tr[j][5]) <= abs(angulo))
            tr[j][7] = 1;
        if (abs(tr[j][6]) <= abs(angulo))
            tr[j][7] = 1;
    }
}

void lado_largo(float tr[][10], int i) // define el lado mas largo
{
    int j(0),k(0);
    float aux(0);
    for (j=1;j<i;j++)
    {
        aux = tr[j][1];
        tr[j][8] = 1;
        for (k=2;k<4;k++)
        {
            if (abs(tr[j][k]) < abs(aux))
            {
                aux = tr[j][k];
                tr[j][8] = k;
            }
        }
    }
}

float buscar_nodo(float node[][3], float x, float y, int i)
{
    int j(0);
    float aux(0);
    for (j=1;j<i;j++)
    {
        if (abs(node[j][1]) == abs(x) && abs(node[j][2]) == abs(y))
            aux = node[j][0];

    }
    return aux;
}


void refinar(int trian[][3],float node[][3], float tr[][10], int ntr[][3], int &i, int &j)
{
    ofstream fr("resultados.mesh");
    float x1(0),x2(0),x3(0),y1(0),y2(0),y3(0),xn(0),yn(0),nn(0);
    int n1(0),n2(0),n3(0),k(0),op(0),nd(0),nd2(0),c(0);
    for(k=1;k<i;k++)
    {
        if (tr[k][7] == 1) {
            n1 = trian[k][0]; //nodos
            n2 = trian[k][1];
            n3 = trian[k][2];

            x1 = node[n1][1]; // ejes x
            x2 = node[n2][1];
            x3 = node[n3][1];

            y1 = node[n1][2]; // ejes y
            y2 = node[n2][2];
            y3 = node[n3][2];
            if (tr[k][8] == 1)
            {
                xn = (x2 + x1)/2;
                yn = (y2 + y1)/2;
                op = n3;
                nd = n1;
                nd2 = n2;
            }
            if (tr[k][8] == 2)
            {
                xn = (x3 + x1)/2;
                yn = (y3 + y1)/2;
                op = n2;
                nd = n1;
                nd2 = n3;
            }
            if (tr[k][8] == 3)
            {
                xn = (x3 + x2)/2;
                yn = (y3 + y2)/2;
                op = n1;
                nd = n2;
                nd2 = n3;
            }
            nn = buscar_nodo(node,xn,yn,j);
            if (nn == 0)
            {
                node[j][0] = j;
                node[j][1] = xn;
                node[j][2] = yn;
                j++;
            }
            ntr[c][0] = nd;
            ntr[c][1] = j;
            ntr[c][2] = op;
            fr << ntr[c][0] << " " << ntr[c][1] << " " << ntr[c][2] << endl;
            c++;
            ntr[c][0] = j;
            ntr[c][1] = nd2;
            ntr[c][2] = op;
            fr << ntr[c][0] << " " << ntr[c][1] << " " << ntr[c][2] << endl;
            c++;
        }
        else
        {
            ntr[c][0] = trian[k][0];
            ntr[c][1] = trian[k][1];
            ntr[c][2] = trian[k][2];
            fr << ntr[c][0] << " " << ntr[c][1] << " " << ntr[c][2] << endl;
            c++;
        }
    }
}

int main()
{
    int trian[20000][3] = {0};
    int ntr[20000][3] = {0};
    float node[9000][3] = {0};
    float tr[20000][10] = {0};
    float angulo(0);
    string linea, aux, aux2;
    int i(0),j(1),pos(0),k(0);
    ifstream fs("espiral.mesh"); //Abre el archivo
    if (fs.fail()) //Verifica si lo encuentra
        cout << "El archivo espiral.mesh no se encuentra." << endl;
    else{
            cout << "Archivo espiral.mesh leido." << endl;
            while (!fs.eof()) {
                getline(fs, linea); //extrae la linea
                if (i!=0){
                //cout << linea << endl; // muestra la linea
                    pos = linea.find(" "); //Encuentra el primer espacio
                    aux = linea.substr(0,pos); //extrae el primer numero
                    trian[i][0] = stoi(aux); // guarda el numero
                    aux2 = linea.substr(pos+1); //extrae el resto del string
                    pos = aux2.find(" "); // encuentra el proximo espacio
                    aux = aux2.substr(0,pos); //extrae el proximo numero
                    trian[i][1] = stoi(aux); // guarda el numero
                    aux = aux2.substr(pos+1); //extrae el ultimo numero
                    trian[i][2] = stoi(aux); // guarda el numero
                }
                i++;
                }
    }
    fs.close(); // cierra el archivo


    ifstream fs2("espiralnode.txt"); //Abre el archivo .txt
    if (fs2.fail()) //Verifica si lo encuentra{
        {
            ifstream fs2("espiral.node"); //Abre el archivo .node
        }
    if (fs2.fail())
            cout << "El archivo espiral.node no se encuentra." << endl;
    else{
            cout << "Archivo espiral.node leido." << endl;
            while (!fs2.eof()) {
                getline(fs2, linea); //extrae la linea
           //     cout << linea << endl; // muestra la linea
                    pos = linea.find(" "); //Encuentra el primer espacio
                    aux = linea.substr(0,pos); //extrae el primer numero
                    node[j][0] = stof(aux); // guarda el numero
                    aux2 = linea.substr(pos+1); //extrae el resto del string
                    pos = aux2.find(" "); // encuentra el proximo espacio
                    aux = aux2.substr(0,pos); //extrae el proximo numero
                    node[j][1] = stof(aux); // guarda el numero
                    aux = aux2.substr(pos+1); //extrae el ultimo numero
                    node[j][2] = stof(aux); // guarda el numero
                    j++;
                }
    }
    fs2.close(); // cierra el archivo

    cout << endl;
    while (angulo == 0 || angulo > 180.0)
    {
        cout << "Ingrese angulo" << endl;
        cin >> angulo;
        if (angulo == 0 || angulo > 180.0)
            cout << "Angulo no valido" << endl;
    }
     cout << endl;

    triangulo(trian,node,tr,i);
    se_refina(tr,angulo,i);
    lado_largo(tr,i);
    refinar(trian,node,tr,ntr,i,j);

/*
    ofstream fr("resultados.txt");
    for (k=1;k<i;k++)
    {
        fr << ntr[k][0] << " " << ntr[k][1] << " " << ntr[k][2] << endl;
    }
    */
    cout << "-----" << endl;
    cout << "Archivo resultados.mesh creado" << endl;

    ofstream fr2("resultados.node");
    for (k=1;k<j;k++)
    {
        fr2 << node[k][0] << " " << node[k][1] << " " << node[k][2] << endl;
    }
    cout << "Archivo resultados.node creado" << endl;

    return 0;
}
