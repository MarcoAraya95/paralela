#include <iostream>
#include <fstream>
#include <math.h>
#include <stdlib.h>
using namespace std;

int main()
{
    char linea[128];
    double A[10000] = {0};
    int i = 0;
    double suma = 0;
    double aux2 = 0;
    double varianza = 0;
    double desv = 0;
    double temp = 0;
    int j = 0;
    double frec[10000] = {0};
    double moda;
    int frecuencia = 0;
    ifstream fs("numeros.txt");
    if(fs.fail())
        cout << "No existe el archivo" << endl;
    else
    {
        while(!fs.eof() and i < 10000)
        {

            fs.getline(linea, sizeof(linea));
            linea[1] =  '.';
            A[i] = fabs(atof(linea));
            suma = suma + A[i];
            i++;
        }
    }
    cout << "La cantidad de datos es: " << i <<  endl;
    cout << "La suma es: " << suma << endl;
    cout << "El promedio es: " << suma/i<< endl;

    for(int aux = 0; aux < i; aux++)
    {
        aux2 = aux2 + pow((A[aux]-(suma/i)),2);
    }
    varianza = aux2/i;
    cout << "La varianza es : " << varianza << endl;
    desv = sqrt(varianza);
    cout << "La desviacion estandar es: " << desv << endl;



    for(int aux = 0; aux < i; aux++)
    {
        temp = A[aux];
        j = aux - 1;
        while ( (A[j] > temp) && (j >= 0) )
        {
            A[j+1] = A[j];
            j--;
        }
    A[j+1] = temp;
    }
    cout << "La mediana es: " << A[i/2] << endl;



    for(int aux = 0; aux < i; aux++)
    {
        for(int aux2 = 0; aux2 < i; aux2++)
        {
            if(fabs(A[aux]) == fabs(A[aux2]))
                frec[aux]++;
        }
    }
    for(int aux = 0; aux < i; aux++)
    {
        if(frec[aux] >= frecuencia)
        {
            frecuencia = frec[aux];
            moda = A[aux];
        }
    }


    cout << "El valor moda es: " << moda << " con frecuencia: " << frecuencia << endl;
}
